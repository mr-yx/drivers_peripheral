# Copyright (c) 2021-2022 Huawei Device Co., Ltd.
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

import("//build/config/features.gni")
import("//build/ohos.gni")
import("//build/test.gni")
import("//drivers/hdf_core/adapter/uhdf2/uhdf.gni")
import("../../camera.gni")

module_output_path = "hdf/camera"

ohos_fuzztest("CameraIpcCameraDeviceRemoteFuzzTest") {
  module_out_path = module_output_path
  fuzz_config_file =
      "//drivers/peripheral/camera/hal/test/fuzz/ipccameradeviceremote_fuzzer"
  cflags = [
    "-g",
    "-O0",
    "-Wno-unused-variable",
    "-fno-omit-frame-pointer",
  ]
  sources = [
    "$camera_path/../interfaces/hdi_ipc/server/src/camera_device_stub.cpp",
    "./ipccameradeviceremote_fuzzer/ipccameradeviceremote_fuzzer.cpp",
  ]

  include_dirs = [
    "./include",
    "$camera_path/test/fuzz/ipccameradeviceremote_fuzzer",
    "$camera_path/../interfaces/include",
    "$camera_path/../interfaces/hdi_ipc",
    "$camera_path/../interfaces/hdi_ipc/utils/include",
    "$camera_path/../interfaces/hdi_ipc/client/include",
    "$camera_path/../interfaces/hdi_ipc/server/include",
    "$camera_path/../interfaces/hdi_ipc/callback/host/include",
    "$camera_path/../interfaces/hdi_ipc/callback/device/include",
    "$camera_path/../interfaces/hdi_ipc/callback/operator/include",
    "$camera_path/include",
    "$camera_path/hdi_impl",
    "$camera_path/hdi_impl/include",
    "$camera_path/hdi_impl/include/camera_host",
    "$camera_path/hdi_impl/include/camera_device",
    "$camera_path/hdi_impl/include/stream_operator",
    "$camera_path/hdi_impl/include/offline_stream_operator",
    "$camera_path/device_manager/include/",
    "$camera_path/device_manager/include/mpi",
    "$camera_path/pipeline_core/utils",
    "$camera_path/pipeline_core/pipeline_impl/include",
    "$camera_path/pipeline_core/host_stream/include",
    "$camera_path/pipeline_core/include",
    "$camera_path/pipeline_core/ipp/include",
    "$camera_path/pipeline_core/nodes/include",
    "$camera_path/utils/event",
    "//drivers/peripheral/camera/interfaces/metadata/include",
    "//drivers/peripheral/camera/hal/hdi_impl/src/stream_operator/stream_tunnel/standard",
    "//drivers/peripheral/camera/hal/hdi_impl/src/stream_operator/stream_tunnel/lite",
    "//drivers/peripheral/display/interfaces/include",
  ]

  deps = [
    "$camera_path/../interfaces/hdi_ipc/client:libcamera_client",
    "$camera_path/buffer_manager:camera_buffer_manager",
    "$camera_path/device_manager:camera_device_manager",
    "$camera_path/hdi_impl:camera_hdi_impl",
    "$camera_path/pipeline_core:camera_pipeline_core",
    "//drivers/peripheral/camera/interfaces/metadata:metadata",
  ]

  if (is_standard_system) {
    external_deps = [
      "drivers_peripheral_display:hdi_display_gralloc",
      "graphic_chipsetsdk:surface",
      "hdf_core:libhdf_host",
      "hdf_core:libhdf_ipc_adapter",
      "hdf_core:libhdf_utils",
      "hdf_core:libhdi",
      "hiviewdfx_hilog_native:libhilog",
      "ipc:ipc_single",
      "utils_base:utils",
    ]
  } else {
    external_deps = [ "hilog:libhilog" ]
  }

  external_deps += [
    "ipc:ipc_single",
    "samgr_standard:samgr_proxy",
  ]

  install_enable = true
  install_images = [ chipset_base_dir ]
  subsystem_name = "hdf"
  part_name = "drivers_peripheral_camera"
}

ohos_fuzztest("CameraIpcCameraHostServiceFuzzTest") {
  module_out_path = module_output_path
  fuzz_config_file =
      "//drivers/peripheral/camera/hal/test/fuzz/ipccamerahostservice_fuzzer"
  cflags = [
    "-g",
    "-O0",
    "-Wno-unused-variable",
    "-fno-omit-frame-pointer",
  ]
  sources = [ "./ipccamerahostservice_fuzzer/ipccamerahostservice_fuzzer.cpp" ]

  include_dirs = [
    "./include",
    "$camera_path/test/fuzz/ipccamerahostservice_fuzzer",
    "$camera_path/../interfaces/include",
    "$camera_path/../interfaces/hdi_ipc/utils/include",
    "$camera_path/../interfaces/hdi_ipc/client/include",
    "$camera_path/../interfaces/hdi_ipc/server/include",
    "$camera_path/../interfaces/hdi_ipc/callback/host/include",
    "$camera_path/../interfaces/hdi_ipc/callback/device/include",
    "$camera_path/../interfaces/hdi_ipc/callback/operator/include",
    "$camera_path/include",
    "$camera_path/hdi_impl",
    "$camera_path/hdi_impl/include",
    "$camera_path/hdi_impl/include/camera_host",
    "$camera_path/hdi_impl/include/camera_device",
    "$camera_path/hdi_impl/include/stream_operator",
    "$camera_path/hdi_impl/include/offline_stream_operator",
    "$camera_path/device_manager/include/",
    "$camera_path/device_manager/include/mpi",
    "$camera_path/pipeline_core/utils",
    "$camera_path/pipeline_core/pipeline_impl/include",
    "$camera_path/pipeline_core/host_stream/include",
    "$camera_path/pipeline_core/include",
    "$camera_path/pipeline_core/ipp/include",
    "$camera_path/pipeline_core/nodes/include",
    "$camera_path/utils/event",
    "//drivers/peripheral/camera/interfaces/metadata/include",
    "//drivers/peripheral/camera/hal/hdi_impl/src/stream_operator/stream_tunnel/standard",
    "//drivers/peripheral/camera/hal/hdi_impl/src/stream_operator/stream_tunnel/lite",
    "//drivers/peripheral/display/interfaces/include",
  ]

  deps = [
    "$camera_path/../interfaces/hdi_ipc/client:libcamera_client",
    "$camera_path/buffer_manager:camera_buffer_manager",
    "$camera_path/device_manager:camera_device_manager",
    "$camera_path/hdi_impl:camera_hdi_impl",
    "$camera_path/pipeline_core:camera_pipeline_core",
    "//drivers/peripheral/camera/interfaces/metadata:metadata",
  ]

  if (is_standard_system) {
    external_deps = [
      "drivers_peripheral_display:hdi_display_gralloc",
      "graphic_chipsetsdk:surface",
      "hdf_core:libhdf_host",
      "hdf_core:libhdf_ipc_adapter",
      "hdf_core:libhdf_utils",
      "hdf_core:libhdi",
      "hiviewdfx_hilog_native:libhilog",
      "ipc:ipc_single",
      "utils_base:utils",
    ]
  } else {
    external_deps = [ "hilog:libhilog" ]
  }

  external_deps += [
    "ipc:ipc_single",
    "samgr_standard:samgr_proxy",
  ]

  install_enable = true
  install_images = [ chipset_base_dir ]
  subsystem_name = "hdf"
  part_name = "drivers_peripheral_camera"
}

ohos_fuzztest("CameraIpcOfflineFuzzTest") {
  module_out_path = module_output_path
  fuzz_config_file =
      "//drivers/peripheral/camera/hal/test/fuzz/ipcoffline_fuzzer"
  cflags = [
    "-g",
    "-O0",
    "-Wno-unused-variable",
    "-fno-omit-frame-pointer",
  ]
  sources = [ "./ipcoffline_fuzzer/ipcoffline_fuzzer.cpp" ]

  include_dirs = [
    "./include",
    "$camera_path/test/fuzz/ipcoffline_fuzzer",
    "$camera_path/../interfaces/include",
    "$camera_path/../interfaces/hdi_ipc/utils/include",
    "$camera_path/../interfaces/hdi_ipc/client/include",
    "$camera_path/../interfaces/hdi_ipc/server/include",
    "$camera_path/../interfaces/hdi_ipc/callback/host/include",
    "$camera_path/../interfaces/hdi_ipc/callback/device/include",
    "$camera_path/../interfaces/hdi_ipc/callback/operator/include",
    "$camera_path/include",
    "$camera_path/hdi_impl",
    "$camera_path/hdi_impl/include",
    "$camera_path/hdi_impl/include/camera_host",
    "$camera_path/hdi_impl/include/camera_device",
    "$camera_path/hdi_impl/include/stream_operator",
    "$camera_path/hdi_impl/include/offline_stream_operator",
    "$camera_path/device_manager/include/",
    "$camera_path/device_manager/include/mpi",
    "$camera_path/pipeline_core/utils",
    "$camera_path/pipeline_core/pipeline_impl/include",
    "$camera_path/pipeline_core/host_stream/include",
    "$camera_path/pipeline_core/include",
    "$camera_path/pipeline_core/ipp/include",
    "$camera_path/pipeline_core/nodes/include",
    "$camera_path/utils/event",
    "//drivers/peripheral/camera/interfaces/metadata/include",
    "//drivers/peripheral/camera/hal/hdi_impl/src/stream_operator/stream_tunnel/standard",
    "//drivers/peripheral/camera/hal/hdi_impl/src/stream_operator/stream_tunnel/lite",
    "//drivers/peripheral/display/interfaces/include",
  ]

  deps = [
    "$camera_path/../interfaces/hdi_ipc/client:libcamera_client",
    "$camera_path/buffer_manager:camera_buffer_manager",
    "$camera_path/device_manager:camera_device_manager",
    "$camera_path/hdi_impl:camera_hdi_impl",
    "$camera_path/pipeline_core:camera_pipeline_core",
    "//drivers/peripheral/camera/interfaces/metadata:metadata",
  ]

  if (is_standard_system) {
    external_deps = [
      "drivers_peripheral_display:hdi_display_gralloc",
      "graphic_chipsetsdk:surface",
      "hdf_core:libhdf_host",
      "hdf_core:libhdf_ipc_adapter",
      "hdf_core:libhdf_utils",
      "hdf_core:libhdi",
      "hiviewdfx_hilog_native:libhilog",
      "utils_base:utils",
    ]
  } else {
    external_deps = [ "hilog:libhilog" ]
  }

  external_deps += [
    "ipc:ipc_single",
    "samgr_standard:samgr_proxy",
  ]

  install_enable = true
  install_images = [ chipset_base_dir ]
  subsystem_name = "hdf"
  part_name = "drivers_peripheral_camera"
}

ohos_fuzztest("CameraIpcStreamOperatorFuzzTest") {
  module_out_path = module_output_path
  fuzz_config_file =
      "//drivers/peripheral/camera/hal/test/fuzz/ipcstreamoperator_fuzzer"
  cflags = [
    "-g",
    "-O0",
    "-Wno-unused-variable",
    "-fno-omit-frame-pointer",
  ]
  sources = [ "./ipcstreamoperator_fuzzer/ipcstreamoperator_fuzzer.cpp" ]

  include_dirs = [
    "./include",
    "$camera_path/test/fuzz/ipcstreamoperator_fuzzer",
    "$camera_path/../interfaces/include",
    "$camera_path/../interfaces/hdi_ipc/utils/include",
    "$camera_path/../interfaces/hdi_ipc/client/include",
    "$camera_path/../interfaces/hdi_ipc/server/include",
    "$camera_path/../interfaces/hdi_ipc/callback/host/include",
    "$camera_path/../interfaces/hdi_ipc/callback/device/include",
    "$camera_path/../interfaces/hdi_ipc/callback/operator/include",
    "$camera_path/include",
    "$camera_path/hdi_impl",
    "$camera_path/hdi_impl/include",
    "$camera_path/hdi_impl/include/camera_host",
    "$camera_path/hdi_impl/include/camera_device",
    "$camera_path/hdi_impl/include/stream_operator",
    "$camera_path/hdi_impl/include/offline_stream_operator",
    "$camera_path/device_manager/include/",
    "$camera_path/device_manager/include/mpi",
    "$camera_path/pipeline_core/utils",
    "$camera_path/pipeline_core/pipeline_impl/include",
    "$camera_path/pipeline_core/host_stream/include",
    "$camera_path/pipeline_core/include",
    "$camera_path/pipeline_core/ipp/include",
    "$camera_path/pipeline_core/nodes/include",
    "$camera_path/utils/event",
    "//drivers/peripheral/camera/interfaces/metadata/include",
    "//drivers/peripheral/camera/hal/hdi_impl/src/stream_operator/stream_tunnel/standard",
    "//drivers/peripheral/camera/hal/hdi_impl/src/stream_operator/stream_tunnel/lite",
    "//drivers/peripheral/display/interfaces/include",
  ]

  deps = [
    "$camera_path/../interfaces/hdi_ipc/client:libcamera_client",
    "$camera_path/buffer_manager:camera_buffer_manager",
    "$camera_path/device_manager:camera_device_manager",
    "$camera_path/hdi_impl:camera_hdi_impl",
    "$camera_path/pipeline_core:camera_pipeline_core",
    "//drivers/peripheral/camera/interfaces/metadata:metadata",
  ]

  if (is_standard_system) {
    external_deps = [
      "drivers_peripheral_display:hdi_display_gralloc",
      "graphic_chipsetsdk:surface",
      "hdf_core:libhdf_host",
      "hdf_core:libhdf_ipc_adapter",
      "hdf_core:libhdf_utils",
      "hdf_core:libhdi",
      "hiviewdfx_hilog_native:libhilog",
      "utils_base:utils",
    ]
  } else {
    external_deps = [ "hilog:libhilog" ]
  }

  external_deps += [
    "ipc:ipc_single",
    "samgr_standard:samgr_proxy",
  ]

  install_enable = true
  install_images = [ chipset_base_dir ]
  subsystem_name = "hdf"
  part_name = "drivers_peripheral_camera"
}

ohos_fuzztest("CameraIpcCameraDeviceCallbackFuzzTest") {
  module_out_path = module_output_path
  fuzz_config_file =
      "//drivers/peripheral/camera/hal/test/fuzz/ipccameradevicecallback_fuzzer"
  cflags = [
    "-g",
    "-O0",
    "-Wno-unused-variable",
    "-fno-omit-frame-pointer",
  ]
  sources =
      [ "./ipccameradevicecallback_fuzzer/ipccameradevicecallback_fuzzer.cpp" ]

  include_dirs = [
    "./include",
    "$camera_path/test/fuzz/ipccameradevicecallback_fuzzer",
    "$camera_path/../interfaces/include",
    "$camera_path/../interfaces/hdi_ipc/utils/include",
    "$camera_path/../interfaces/hdi_ipc/client/include",
    "$camera_path/../interfaces/hdi_ipc/server/include",
    "$camera_path/../interfaces/hdi_ipc/callback/host/include",
    "$camera_path/../interfaces/hdi_ipc/callback/device/include",
    "$camera_path/../interfaces/hdi_ipc/callback/operator/include",
    "$camera_path/include",
    "$camera_path/hdi_impl",
    "$camera_path/hdi_impl/include",
    "$camera_path/hdi_impl/include/camera_host",
    "$camera_path/hdi_impl/include/camera_device",
    "$camera_path/hdi_impl/include/stream_operator",
    "$camera_path/hdi_impl/src/stream_operator/stream_tunnel/standard",

    "$camera_path/hdi_impl/include/offline_stream_operator",
    "$camera_path/device_manager/include/",
    "$camera_path/device_manager/include/mpi",
    "$camera_path/pipeline_core/utils",
    "$camera_path/pipeline_core/pipeline_impl/include",
    "$camera_path/pipeline_core/host_stream/include",
    "$camera_path/pipeline_core/include",
    "$camera_path/pipeline_core/ipp/include",
    "$camera_path/pipeline_core/nodes/include",
    "$camera_path/utils/event",
    "//drivers/peripheral/camera/interfaces/metadata/include",
    "//drivers/peripheral/camera/hal/hdi_impl/src/stream_operator/stream_tunnel/standard",
    "//drivers/peripheral/camera/hal/hdi_impl/src/stream_operator/stream_tunnel/lite",
    "//drivers/peripheral/display/interfaces/include",
  ]

  deps = [
    "$camera_path/../interfaces/hdi_ipc/client:libcamera_client",
    "$camera_path/buffer_manager:camera_buffer_manager",
    "$camera_path/device_manager:camera_device_manager",
    "$camera_path/hdi_impl:camera_hdi_impl",
    "$camera_path/pipeline_core:camera_pipeline_core",
    "//drivers/peripheral/camera/interfaces/metadata:metadata",
  ]

  if (is_standard_system) {
    external_deps = [
      "drivers_peripheral_display:hdi_display_gralloc",
      "graphic_chipsetsdk:surface",
      "hdf_core:libhdf_host",
      "hdf_core:libhdf_ipc_adapter",
      "hdf_core:libhdf_utils",
      "hdf_core:libhdi",
      "hiviewdfx_hilog_native:libhilog",
      "utils_base:utils",
    ]
  } else {
    external_deps = [ "hilog:libhilog" ]
  }

  external_deps += [
    "ipc:ipc_single",
    "samgr_standard:samgr_proxy",
  ]

  install_enable = true
  install_images = [ chipset_base_dir ]
  subsystem_name = "hdf"
  part_name = "drivers_peripheral_camera"
}

ohos_fuzztest("CameraIpcCameraHostCallbackFuzzTest") {
  module_out_path = module_output_path
  fuzz_config_file =
      "//drivers/peripheral/camera/hal/test/fuzz/ipccamerahostcallback_fuzzer"
  cflags = [
    "-g",
    "-O0",
    "-Wno-unused-variable",
    "-fno-omit-frame-pointer",
  ]
  sources =
      [ "./ipccamerahostcallback_fuzzer/ipccamerahostcallback_fuzzer.cpp" ]

  include_dirs = [
    "./include",
    "$camera_path/test/fuzz/ipccamerahostcallback_fuzzer",
    "$camera_path/../interfaces/include",
    "$camera_path/../interfaces/hdi_ipc/utils/include",
    "$camera_path/../interfaces/hdi_ipc/client/include",
    "$camera_path/../interfaces/hdi_ipc/server/include",
    "$camera_path/../interfaces/hdi_ipc/callback/host/include",
    "$camera_path/../interfaces/hdi_ipc/callback/device/include",
    "$camera_path/../interfaces/hdi_ipc/callback/operator/include",
    "$camera_path/include",
    "$camera_path/hdi_impl",
    "$camera_path/hdi_impl/include",
    "$camera_path/hdi_impl/include/camera_host",
    "$camera_path/hdi_impl/include/camera_device",
    "$camera_path/hdi_impl/include/stream_operator",
    "$camera_path/hdi_impl/include/offline_stream_operator",
    "$camera_path/device_manager/include/",
    "$camera_path/device_manager/include/mpi",
    "$camera_path/pipeline_core/utils",
    "$camera_path/pipeline_core/pipeline_impl/include",
    "$camera_path/pipeline_core/host_stream/include",
    "$camera_path/pipeline_core/include",
    "$camera_path/pipeline_core/ipp/include",
    "$camera_path/pipeline_core/nodes/include",
    "$camera_path/utils/event",
    "//drivers/peripheral/camera/interfaces/metadata/include",
    "//drivers/peripheral/camera/hal/hdi_impl/src/stream_operator/stream_tunnel/standard",
    "//drivers/peripheral/camera/hal/hdi_impl/src/stream_operator/stream_tunnel/lite",
    "//drivers/peripheral/display/interfaces/include",
  ]

  deps = [
    "$camera_path/../interfaces/hdi_ipc/client:libcamera_client",
    "$camera_path/buffer_manager:camera_buffer_manager",
    "$camera_path/device_manager:camera_device_manager",
    "$camera_path/hdi_impl:camera_hdi_impl",
    "$camera_path/pipeline_core:camera_pipeline_core",
    "//drivers/peripheral/camera/interfaces/metadata:metadata",
  ]

  if (is_standard_system) {
    external_deps = [
      "drivers_peripheral_display:hdi_display_gralloc",
      "graphic_chipsetsdk:surface",
      "hdf_core:libhdf_host",
      "hdf_core:libhdf_ipc_adapter",
      "hdf_core:libhdf_utils",
      "hdf_core:libhdi",
      "hiviewdfx_hilog_native:libhilog",
      "utils_base:utils",
    ]
  } else {
    external_deps = [ "hilog:libhilog" ]
  }

  external_deps += [
    "ipc:ipc_single",
    "samgr_standard:samgr_proxy",
  ]

  install_enable = true
  install_images = [ chipset_base_dir ]
  subsystem_name = "hdf"
  part_name = "drivers_peripheral_camera"
}

ohos_fuzztest("CameraIpcStreamOperatorCallbackFuzzTest") {
  module_out_path = module_output_path
  fuzz_config_file = "//drivers/peripheral/camera/hal/test/fuzz/ipcstreamoperatorcallback_fuzzer"
  cflags = [
    "-g",
    "-O0",
    "-Wno-unused-variable",
    "-fno-omit-frame-pointer",
  ]
  sources = [
    "./ipcstreamoperatorcallback_fuzzer/ipcstreamoperatorcallback_fuzzer.cpp",
  ]

  include_dirs = [
    "./include",
    "$camera_path/test/fuzz/ipcstreamoperatorcallback_fuzzer",
    "$camera_path/../interfaces/include",
    "$camera_path/../interfaces/hdi_ipc/utils/include",
    "$camera_path/../interfaces/hdi_ipc/client/include",
    "$camera_path/../interfaces/hdi_ipc/server/include",
    "$camera_path/../interfaces/hdi_ipc/callback/host/include",
    "$camera_path/../interfaces/hdi_ipc/callback/device/include",
    "$camera_path/../interfaces/hdi_ipc/callback/operator/include",
    "$camera_path/include",
    "$camera_path/hdi_impl",
    "$camera_path/hdi_impl/include",
    "$camera_path/hdi_impl/include/camera_host",
    "$camera_path/hdi_impl/include/camera_device",
    "$camera_path/hdi_impl/include/stream_operator",
    "$camera_path/hdi_impl/include/offline_stream_operator",
    "$camera_path/device_manager/include/",
    "$camera_path/device_manager/include/mpi",
    "$camera_path/pipeline_core/utils",
    "$camera_path/pipeline_core/pipeline_impl/include",
    "$camera_path/pipeline_core/host_stream/include",
    "$camera_path/pipeline_core/include",
    "$camera_path/pipeline_core/ipp/include",
    "$camera_path/pipeline_core/nodes/include",
    "$camera_path/utils/event",
    "//drivers/peripheral/camera/interfaces/metadata/include",
    "//drivers/peripheral/camera/hal/hdi_impl/src/stream_operator/stream_tunnel/standard",
    "//drivers/peripheral/camera/hal/hdi_impl/src/stream_operator/stream_tunnel/lite",
    "//drivers/peripheral/display/interfaces/include",
  ]

  deps = [
    "$camera_path/../interfaces/hdi_ipc/client:libcamera_client",
    "$camera_path/buffer_manager:camera_buffer_manager",
    "$camera_path/device_manager:camera_device_manager",
    "$camera_path/hdi_impl:camera_hdi_impl",
    "$camera_path/pipeline_core:camera_pipeline_core",
    "//drivers/peripheral/camera/interfaces/metadata:metadata",
  ]

  if (is_standard_system) {
    external_deps = [
      "drivers_peripheral_display:hdi_display_gralloc",
      "graphic_chipsetsdk:surface",
      "hdf_core:libhdf_host",
      "hdf_core:libhdf_ipc_adapter",
      "hdf_core:libhdf_utils",
      "hdf_core:libhdi",
      "hiviewdfx_hilog_native:libhilog",
      "utils_base:utils",
    ]
  } else {
    external_deps = [ "hilog:libhilog" ]
  }

  external_deps += [
    "ipc:ipc_single",
    "samgr_standard:samgr_proxy",
  ]

  install_enable = true
  install_images = [ chipset_base_dir ]
  subsystem_name = "hdf"
  part_name = "drivers_peripheral_camera"
}

group("camera_hal_fuzztest") {
  testonly = true
  deps = [
    #":CameraIpcCameraDeviceCallbackFuzzTest",
    ":CameraIpcCameraDeviceRemoteFuzzTest",
    ":CameraIpcCameraHostCallbackFuzzTest",
    ":CameraIpcCameraHostServiceFuzzTest",
    ":CameraIpcOfflineFuzzTest",
    ":CameraIpcStreamOperatorCallbackFuzzTest",
    ":CameraIpcStreamOperatorFuzzTest",
  ]
}
